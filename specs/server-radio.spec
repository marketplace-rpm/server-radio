Name:                           server-radio
Version:                        1.0.0
Release:                        4%{?dist}
Summary:                        SERVER-package for install and configure radio
License:                        GPLv3

Requires:                       meta-system meta-icecast meta-ezstream meta-nginx

%description
SERVER-package for install and configure radio.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%install


%files


%changelog
* Thu Jul 04 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-4
- Update from MARKETPLACE.

* Sun Mar 31 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- New version: 1.0.0-3.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- New version: 1.0.0-2.

* Wed Feb 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
