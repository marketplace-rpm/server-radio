# Information / Информация

SPEC-файл для создания RPM-пакета **server-radio**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/common-server`.
2. Установить пакет: `dnf install server-radio`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)